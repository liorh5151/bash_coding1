#!/usr/bin/env bash

###################
# Created by Lior Hacohen
# Purpose: to work with positional params
# Version: 0.0.1
###################

pose_var=$5

echo "The $0 has run and the value os pos_var is $pose_var"
# "$0" --> the name of the script itself --> "par2.sh"
# The variable "pose_var" is set dto the $5 which does'nt exists, the scrip[t will ignore it.
