#!/usr/bin/env bash

###################
# Created by Lior Hacohen
# Purpose: to work with positional params
# Version: 0.0.1
###################

#var=$10
#echo "$0 has gotten the variable $10"

"""
when we will do the following script:
var=$10
echo "$0 has gotten the variable $10"
And then run the script with the folloeint inputs:
./par3.sh 1 2 3 4 5 6 7 8 9 9 
We will get - /par3.sh has gotten the variable 10
Why 10?
Because the script takes the first variable (which is 1) and add a 0 to it.
How to fix it?
We define out variables with "{}".
For example:
var=${10}
echo "$0 has gotten the variable ${10}"
"""


# Many times we will need to debug our code.
