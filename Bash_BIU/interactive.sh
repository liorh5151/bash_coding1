#!/usr/bin/env bash

###################
# Created by Lior Hacohen
# Purpose: interactive script (how to save a user's input - like the built-in input() function in Python).
# Version: 0.0.1
###################

echo "Please provide your name:"
# This script is asking the user for a name (which will by our "$name" later).
read name 
# By this action we will save the user's unput into a variable called "name".

echo "Hello $name"
# Here we will print "Hello" and the value of the variable "name" (which is the user's input) after.
