#!/usr/bin/env bash

##############################
# Created by: Lior Hacohen
# Purpose: multiple param input
# Version 0.0.1
##############################

multi=$@
# We defined that the "$" value sign will include all the variables that we will deliver.

echo -e "The value passed were: \n $multi"
# This action will print any value given, regardless how many arguments given.
# The "-e" option in the "echo" command will allow us to do output manipulation like the '\n' option (more info below).

# If "-e" is in effect, the following sequences are recognized:
#\\ --> backslash
#\a --> alert (BEL)
#\b --> backspace
#\c --> produce no further output
#\e --> escape
#\f --> form feed
#\n -->new line
#\r --> carriage return
#\t --> horizontal tab
#\v --> vertical tab
#\0NNN --> byte with octal value NNN (1 to 3 digits)
#\xHH --> byte with hexadecimal value HH (1 to 2 digits)




