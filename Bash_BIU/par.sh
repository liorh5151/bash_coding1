#!/usr/bin/env bash

###################
# Created by Lior Hacohen
# Purpose: to work with positional params
# Version: 0.0.1
###################

name=$1
lname=$2
age=$3


echo "Hello : my name is $name $lname and I am $age years old."
