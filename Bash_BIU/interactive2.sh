#!/usr/bin/env bash

###################
# Created by Lior Hacohen
# Purpose: interactive script (how to save a user'e input - like the input() in Python).
# Version: 0.0.1
###################

read -p "Please provide your name:" name 
# Template --> read -p "what_the_user_will_see" variable_name


echo "Hello $name"
# Here we will print "Hello" and the value of the variable "name" (which is the user's input) after.
