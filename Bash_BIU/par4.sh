#!/usr/bin/env bash

########################
# Created by Lior Hacohen
# Purpose: to work with positional params
# Version: 0.0.1
########################
# Note --> Bash script is space sensitive.


# Defining variables.
name=$1
lname=$2 
age=$4


shift 3 # More about the command below.
# The command "shift 3" is a built-in command in bash (more info down below).
# We can use this command to pass/ingore a variable in (variable number $3).
# If we won't put variable number in the "shift" command (the default is the first variable).


echo "Hello : my name is $name $lname and I am $age years old."