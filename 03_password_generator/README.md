# Shell Script Password Generator

Welcome to `Shell Script Password Generator` project. The use case of this utility is to provide you with automation of generating password based on the amount of digits you are required.

Create shell script that will generate the random password based on amount characters that you provide to it.

- Script should provide us with random password.
- Provide us with encrypting and decrypting that same password.
- Provide us with capability to saving the passwords to local database.
