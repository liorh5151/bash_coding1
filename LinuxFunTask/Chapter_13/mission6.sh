#!/bin/bash

# The mission is: Echo it worked when touch test42 works, and echo it failed when the touch failed. All
# on one command line as a normal user (not root). Test this line in your home directory and
# in /bin/


#To create the test42.txt file:
#touch test42.txt

#This is the script (the answer):


File=test42.txt

if [ -f "$File"  ]; then

	echo "YAYYY it works!!"

else
	echo "OOOPS.. it failed"

fi